try:
    from day6.hw1_day6_deykin_alex import Calculator, is_int
except:
    from hw1_day6_deykin_alex import Calculator, is_int


class AbstractOptimiser:
    def process(self, graph):
        g = self.pre_process(graph)
        result = self.process_internal(g)
        return result, self.post_process(result)

    def pre_process(self, graph):
        return graph

    def process_internal(self, graph):
        return graph

    def post_process(self, graph):
        calc = Calculator(graph=graph)
        return str(calc)

        # def recurse_opt(internal_graph=None):
        #     if isinstance(internal_graph, tuple):
        #         if len(internal_graph) == 1:
        #             return "(" + recurse_opt(internal_graph[0]) + ")"
        #         elif len(internal_graph) == 3:
        #             return recurse_opt(internal_graph[0]) + internal_graph[1] + recurse_opt(
        #                 internal_graph[2])
        #         elif len(internal_graph) == 2 and internal_graph[0] == '~':
        #             if isinstance(internal_graph[1], tuple):
        #                 return '-' + "(" + recurse_opt(internal_graph[1]) + ")"
        #             else:
        #                 return '-' + recurse_opt(internal_graph[1])
        #         else:
        #             raise Exception("Can not process, bad len")
        #     else:
        #         if isinstance(internal_graph, str):
        #             return internal_graph
        #         else:
        #             raise Exception("Can not process, elem is not str or tuple")

        # return recurse_opt(graph)


class DoubleNegativeOptimiser(AbstractOptimiser):
    # -(-a) -> a
    def process_internal(self, graph):
        def recurse_opt(internal_graph=None):
            if isinstance(internal_graph, tuple):
                if len(internal_graph) == 1:
                    return recurse_opt(internal_graph[0])
                elif len(internal_graph) == 3:
                    if internal_graph[1] == '-' and isinstance(internal_graph[2], tuple) and \
                            len(internal_graph[2]) == 2 and internal_graph[2][0] == '~':
                        return (recurse_opt(internal_graph[0]), '+', recurse_opt(internal_graph[2][1]))
                    elif internal_graph[1] == '+' and isinstance(internal_graph[2], tuple) and \
                            len(internal_graph[2]) == 2 and internal_graph[2][0] == '~':
                        return (recurse_opt(internal_graph[0]), '-', recurse_opt(internal_graph[2][1]))
                    else:
                        return (recurse_opt(internal_graph[0]), internal_graph[1], recurse_opt(internal_graph[2]))
                elif len(internal_graph) == 2:
                    if internal_graph[0] == '~' and isinstance(internal_graph[1], tuple) and \
                            len(internal_graph[1]) == 2 and internal_graph[1][0] == '~':
                        return recurse_opt(internal_graph[1][1])
                    else:
                        return (internal_graph[0], recurse_opt(internal_graph[1]))
                else:
                    raise Exception("Can not calc, bad len")
            else:
                if isinstance(internal_graph, str):
                    return internal_graph
                else:
                    raise Exception("Can not process, elem is not str or tuple")

        cur_graph = graph
        opt_graph = recurse_opt(graph)
        while cur_graph != opt_graph:
            cur_graph = opt_graph
            opt_graph = recurse_opt(cur_graph)
        return opt_graph


class IntegerCostantsOptimiser(AbstractOptimiser):
    # a + 4*2 -> a + 8
    def process_internal(self, graph):
        calc = Calculator()

        def recurse_opt(internal_graph=None):
            if isinstance(internal_graph, tuple):
                if len(internal_graph) == 1:
                    return recurse_opt(internal_graph[0])
                elif len(internal_graph) == 3:
                    a = internal_graph[0]
                    b = internal_graph[2]
                    op = internal_graph[1]
                    if is_int(a) and is_int(b):
                        return str(
                            int(calc.operators[op](int(a), int(b))))

                    elif is_int(b) and isinstance(a, tuple) and len(a) == 3 \
                            and calc.priors[op] == calc.priors[a[1]]:
                        if is_int(a[0]):
                            return (str(int(
                                calc.operators[op](int(a[0]), int(b)))), a[1], recurse_opt(a[2]))
                        elif is_int(a[2]):
                            return (recurse_opt(a[2]), a[1],
                                    str(int(calc.operators[op](int(a[2]), int(b)))))
                    else:
                        return (recurse_opt(a), op, recurse_opt(b))
                elif len(internal_graph) == 2:
                    if is_int(internal_graph[1]):
                        return (calc.operators[internal_graph[1]](recurse_opt(internal_graph[1])))
                    else:
                        return (internal_graph[0], recurse_opt(internal_graph[1]))
                else:
                    raise Exception("Can not calc, bad len")
            else:
                if isinstance(internal_graph, str):
                    return internal_graph
                else:
                    raise Exception("Can not process, elem is not str or tuple\n" + str(internal_graph))

        cur_graph = graph
        opt_graph = recurse_opt(graph)
        while cur_graph != opt_graph:
            cur_graph = opt_graph
            opt_graph = recurse_opt(cur_graph)
        return opt_graph


class UnnecessaryOperationsOptimiser(AbstractOptimiser):
    # a * 0 -> 0
    # a + 0 -> 0
    # *   a or True -> True
    # *   a and False -> False
    # unnes = [
    #     ('a', "+", 0, 'a'),
    #     ('a', "*", 1, 'a'),
    #
    #     ("^", 0, 1),
    #     ("*", 0, 0),
    #
    #     ('a', "-", "a", 0),
    #     ('a', "/", 'a', 1),
    #     ('a', "+", 'a', 2 * 'a'),
    # ]

    def process_internal(self, graph):
        def recurse_opt(internal_graph=None):
            print(internal_graph)
            if isinstance(internal_graph, tuple):
                if len(internal_graph) == 1:
                    return recurse_opt(internal_graph[0])
                elif len(internal_graph) == 3:
                    a = internal_graph[0]
                    b = internal_graph[2]
                    op = internal_graph[1]

                    op_b = (op, b)
                    a_op = (a, op)
                    if op_b in [('+', '0'), ('*', '1')]:
                        return recurse_opt(a)
                    elif a_op in [('0', '+'), ('1', '*')]:
                        return recurse_opt(b)

                    elif op_b == ('^', '0'):
                        return '1'
                    elif op_b == ('*', '0'):
                        return '0'
                    elif a == b:
                        if op in '-':
                            return '0'
                        elif op == '/':
                            return '1'
                        elif op == '+':
                            return ('2', '*', recurse_opt(a))
                        else:
                            return (recurse_opt(a), op, recurse_opt(b))
                    elif isinstance(a, tuple) and len(a) == 3 and a[1] == '*' and op == '+':
                        if a[0] == b and is_int(a[2]):
                            return (str(int(a[2]) + 1), '*', b)
                        elif a[2] == b and is_int(a[0]):
                            return (str(int(a[0]) + 1), '*', b)
                    elif isinstance(a, tuple) and len(a) == 2 and a[0] == '~' and op == '^' \
                            and is_int(b) and not int(b) % 2:
                        return (a[1], op, b)
                    else:
                        return (recurse_opt(a), op, recurse_opt(b))

                elif len(internal_graph) == 2:
                    return (internal_graph[0], recurse_opt(internal_graph[1]))
                else:
                    raise Exception("Can not calc, bad len")
            else:
                if isinstance(internal_graph, str):
                    return internal_graph
                else:
                    raise Exception("Can not process, elem is not str or tuple")

        cur_graph = graph
        opt_graph = recurse_opt(graph)
        while cur_graph != opt_graph:
            cur_graph = opt_graph
            opt_graph = recurse_opt(cur_graph)
        return opt_graph


class SimplifierOptimiser(AbstractOptimiser):
    pass


def dev_test():
    # graph = ('a', '*', ((('b', '+', (('var', '/', (('2', '+', 'j'),)), '+', 'c')), '+', '7'),))
    # s = '-(a+b+5)+c-(-(-d))'
    # s="-(-(-(-(-f))))"
    s = "-(a+b)"
    print("s:", s)
    calc = Calculator(expression=s, optimisers=[DoubleNegativeOptimiser()])
    print("optimise:", calc.optimise())


def test1():
    double_negate_tests = [
        ('-(-var)', 'var'),
        ('-(-5)', '5'),
        ('-(a+b)+c-(-d)', 'ab+-c+d+'),
        ('-(-(-(-(-f))))', 'f-'),
        ('-(-(-(-(f))))', 'f'),
        ('-(a+b)', 'ab+-')
    ]
    print("Ex_1".center(50, "_"))

    suc_count = 0
    for num, (case, exp) in enumerate(double_negate_tests):
        # print("\n")
        print("validate_test_{}/{}".center(25, "_").format(num + 1, len(double_negate_tests)))
        tokens = case
        result = None
        # try:
        calculator = Calculator(tokens, optimisers=[DoubleNegativeOptimiser()])
        calculator.optimise()
        # except Exception as ex:
        #     print("Exceptin:", ex)

        if str(calculator) == exp:
            suc_count += 1
        else:
            print('Error in case for "{}". Actual "{}", expected {}'.format(case, str(calculator), exp))
    print("".center(25, "_"))
    print("Result {}/{}".format(suc_count, len(double_negate_tests)))


def test2():
    # test cases помеченные (*) не обязательны к прохождению.
    integer_constant_optimiser_tests = [
        (['1'], ['1']),
        (['1', '+', '2'], ['3']),
        (['1', '-', '2'], ['1-']),
        (['2', '*', '2'], ['4']),
        (['2', '/', '2'], ['1']),
        (['2', '^', '10'], ['1024']),
        (['a', '+', '2', '*', '4'], ['a8+', '8a+']),
        (['2', '+', 'a', '+', '3'], ['5a+', 'a5+']),  # (*)
    ]
    print("Ex_2".center(50, "_"))

    suc_count = 0
    for num, (case, exp) in enumerate(integer_constant_optimiser_tests):
        # print("\n")
        print("validate_test_{}/{}".center(25, "_").format(num + 1, len(integer_constant_optimiser_tests)))
        # try:
        calc = Calculator(case, optimisers=[DoubleNegativeOptimiser(), IntegerCostantsOptimiser()])
        calc.optimise()
        # except Exception as ex:
        #     print("Exceptin:", ex)

        if str(calc) in exp:
            suc_count += 1
        else:
            print('Error in case for "{}". Actual "{}", expected {}'.format(case, str(calc), exp))
    print("".center(25, "_"))
    print("Result {}/{}".format(suc_count, len(integer_constant_optimiser_tests)))


def test3():
    # test cases помеченные (*) не обязательны к прохождению.
    simplifier_optimiser_test = [
        ('a+0', ['a']),
        ('a*1', ['a']),
        ('a*0', ['0']),
        ('b/b', ['1']),
        ('a-a', ['0']),
        ('a+(b-b)', ['a']),
        ('a+(7-6-1)', ['a']),
        ('a^0', ['1']),
        ('a-(-(-a))', ['0']),
        ('(-a)^2', [('a2^')]),
        ('-(-a)^3', [('a3^')]),
        ('a+(-a+a)-a+a+a+a', ['a3*', '3a*']),  # (*)
        ('(a-b)-(a-b)', ['0']),  # (*)
        ('(a-b)/(a-b)', ['1']),  # (*)
        ('(a+b)+(a+b)', ['ab+2*', '2ab+*']),  # (*)
        ('a*b+a*b', ['2ab**', '2ba**', 'a2b**', 'ab2**', 'b2a**', 'ba2**', '2a*b*', '2b*a*', 'a*2b*', 'b*2a*']),  # (*)
    ]
    print("Ex_3".center(50, "_"))

    suc_count = 0
    for num, (case, exp) in enumerate(simplifier_optimiser_test):
        # print("\n")
        print("validate_test_{}/{}".center(10, "_").format(num + 1, len(simplifier_optimiser_test)))
        # try:
        calc = Calculator(case,
                          optimisers=[DoubleNegativeOptimiser(), IntegerCostantsOptimiser(),
                                      UnnecessaryOperationsOptimiser()])
        calc.optimise()
        # except Exception as ex:
        #     print("Exceptin:", ex)

        if str(calc) in exp:
            suc_count += 1
        else:
            print('Error in case for "{}". Actual "{}", expected {}'.format(case, str(calc), exp))
    print("".center(25, "_"))
    print("Result {}/{}".format(suc_count, len(simplifier_optimiser_test)))


# dev_test()
test1()
test2()
test3()
