import collections

def brackets_trim(input_data):
    priors = {'(': 0,
              ')': 1,
              '+': 2,
              '-': 2,
              '*': 3,
              '/': 3,
              '^': 4,
              '': 4}
    opstack = []
    inv_pol = []
    inp_dq = collections.deque([*input_data])
    while ' ' in inp_dq:
        inp_dq.remove(' ')

    while inp_dq:
        ch = inp_dq.popleft()

        if ch == '(':
            opstack.append(ch)
        elif ch == ')':
            while opstack and opstack[-1] != '(':
                inv_pol.append((opstack.pop()))
            opstack.pop()

        elif ch not in priors:
            inv_pol.append(ch)
        else:
            if not opstack:
                opstack.append(ch)
            else:
                while opstack and priors[opstack[-1]] >= priors[ch]:
                    inv_pol.append(opstack.pop())

                opstack.append(ch)
    while opstack:
        inv_pol.append(opstack.pop())

    stack = []
    ip_dq = collections.deque(inv_pol)
    ops = []
    while ip_dq:
        ch = ip_dq.popleft()
        if ch in priors:

            b = stack.pop()
            a = stack.pop()

            op = ops.pop()
            if priors[op] < priors[ch] or (ch != op and(ch in '+-/') and priors[op] == priors[ch]):
                b = "(" + b + ")"

            if priors[ops.pop()] < priors[ch]:
                a = "(" + a + ")"

            ab = a + ch + b

            ops.append(ch)
            stack.append(ab)
        else:
            ops.append('')
            stack.append(ch)
    print("inv_pol:'{}'".format(" ".join(inv_pol)))
    return " ".join(stack)

def tests():
    tests = [("(a*(b/c)+((d-f)/k))", "a*b/c+(d-f)/k"),
             ("a", "a"),
             ("(a)", "a"),
             ("a*(b+c)", "a*(b+c)"),
             ("a+(b+c)", "a+b+c"),
             ("(a*(b/c)+((d-f)/k))*(h*(g-r))", "(a*b/c+(d-f)/k)*h*(g-r)"),
             ("(x+y)/(j*z)+g", "(x+y)/(j*z)+g"),
             ("(x*y)/(j*z)+g", "x*y/(j*z)+g"),
             ("(x*y)-(j*z)+g", "x*y-j*z+g"),
             ("(x*y)+(j*z)+g", "x*y+j*z+g"),
             ("a-(b+c)", "a-(b+c)"),
             ("", ""), ]

    for i, (dat, sans) in enumerate(tests):
        print("TEST{}".format(i).center(25, "_"))
        ans = brackets_trim(dat)
        print("dat:'{}'".format(dat))
        print("sans:'{}'".format(sans))
        print("ans:'{}'".format(ans))
        if ans == sans:
            print("OK!!")
        else:
            print("WRONG!!")