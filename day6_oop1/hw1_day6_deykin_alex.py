import collections
import operator


def is_int(s):
    try:
        int(s)
        return True
    except:
        return False


class Calculator:
    def __init__(self, expression="0", graph=None, operators_priors=None, operators=None, optimisers=None):
        self.optimisers = optimisers
        self.priors = operators_priors if operators_priors else {
            '(': 0,
            ')': 1,
            '+': 2,
            '-': 2,
            '*': 3,
            '/': 3,
            '^': 4,
            '~': 4,
            '': 4
        }
        self.operators = operators if operators else {
            '+': operator.add,
            '-': operator.sub,
            '*': operator.mul,
            '/': operator.truediv,
            '^': operator.pow,
            '~': operator.neg,
        }
        if graph:
            self.graph = graph
            self.expression = self.graph_to_infix()
            # print("expr: ", self.expression)
            self.inv_pol = self.build_inv_pol()

        else:
            self.expression = expression
            try:
                self.inv_pol = self.build_inv_pol()
                self.graph = self.build_graph()
            except Exception as ex:
                print("Cant build graph")
                self.inv_pol = None
                self.graph = None
        # if self.inv_pol:
        #     return "".join(self.inv_pol)

    def __str__(self) -> str:
        if self.inv_pol:
            cur_str = "".join(self.inv_pol)
            while True:
                rep_str = cur_str.replace('~', '-')
                if cur_str == rep_str:
                    break
                cur_str = rep_str
            return cur_str

    def graph_to_infix(self):
        if self.graph:
            # print("graph_to_infix:", self.graph)
            def recurse_opt(internal_graph=None):
                if isinstance(internal_graph, tuple):
                    if len(internal_graph) == 1:
                        return "(" + recurse_opt(internal_graph[0]) + ")"
                    elif len(internal_graph) == 3:
                        a = internal_graph[0]
                        op = internal_graph[1]
                        b = internal_graph[2]
                        if isinstance(b, tuple) and self.priors[op] > self.priors[b[1]]:
                            return recurse_opt(a) + op + "(" + recurse_opt(b) + ")"
                        else:
                            return recurse_opt(a) + op + recurse_opt(b)

                    elif len(internal_graph) == 2:
                        if isinstance(internal_graph[1], tuple) and len(internal_graph[1]) == 3 \
                                and self.priors[internal_graph[0]] > self.priors[internal_graph[1][1]]:

                            return "(" + internal_graph[0] + "(" + recurse_opt(internal_graph[1]) + "))"
                        else:
                            return "(" + internal_graph[0] + recurse_opt(internal_graph[1]) + ")"
                    else:
                        raise Exception("Can not process, bad len")
                else:
                    if isinstance(internal_graph, str):
                        return internal_graph
                    else:
                        raise Exception("Can not process, elem is not str or tuple")

            return recurse_opt(self.graph)
        return "None"

    def optimise(self):
        for optimiser in self.optimisers:
            # print("".center(85, '_'))
            # print("Optimiser:", optimiser)
            # print("".center(25, '_'))
            self.graph, result = optimiser.process(self.graph)
            # print("optimised:", self.graph)
        self.expression = self.graph_to_infix()
        self.inv_pol = self.build_inv_pol()
        return result

    def validate(self) -> bool:
        if not self.graph:
            return False
        chk_ops = self.check_operations(self.graph)
        chk_blns = self.check_balance()

        # print("check_ops:    ", chk_ops)
        # print("check_balance:", chk_blns)
        return chk_ops and chk_blns

    def check_balance(self):
        brstack = []
        bropen = 0
        for ch in self.expression:
            if ch == '(':
                bropen += 1
            elif ch == ')':
                if not bropen:
                    return False
                else:
                    bropen -= 1
        return not bropen

    def build_inv_pol(self):
        # print("make inv_pol for:", self.expression)

        opstack = []
        inv_pol = []
        inp_dq = collections.deque([*self.expression])
        while ' ' in inp_dq:
            inp_dq.remove(' ')
        last_ch = ""
        while inp_dq:
            ch = inp_dq.popleft()
            # print("opstack:", opstack)
            # print("priors:", self.priors)
            if ch == '(':
                opstack.append(ch)
            elif ch == ')':
                while opstack and opstack[-1] != '(':
                    inv_pol.append((opstack.pop()))
                opstack.pop()

            elif ch not in self.priors:  # or (last_ch in '(' and ch == '-'):
                var = ch
                while inp_dq:
                    new_last_ch = ch
                    ch = inp_dq.popleft()
                    if ch not in self.priors:
                        var = var + ch
                    else:
                        inp_dq.appendleft(ch)
                        ch = new_last_ch
                        break
                # print("var    :", var)
                inv_pol.append(var)
            else:
                if ch == "-" and last_ch in self.priors and last_ch != ')':
                    ch = '~'
                    # print("last_ch:", last_ch)
                if not opstack:
                    opstack.append(ch)
                else:
                    while opstack and self.priors[opstack[-1]] >= self.priors[ch]:
                        inv_pol.append(opstack.pop())
                    opstack.append(ch)
            last_ch = ch
        while opstack:
            ch = opstack.pop()
            if ch not in "()":
                inv_pol.append(ch)
        # print("inv_pol_build_graph:", "".join(inv_pol))
        # print("from_expr:", self.expression)
        return inv_pol

    def build_graph(self):

        # print("make_graph".center(25, "_"))
        stack = []
        stack_g = []
        ip_dq = collections.deque(self.inv_pol)
        ops = []
        while ip_dq:
            ch = ip_dq.popleft()
            if ch in self.priors:
                if ch == '~':
                    ab = "(-" + (stack.pop() if stack else '') + ")"
                    ab_g = ('~', stack_g.pop() if stack_g else '')
                else:
                    b = stack.pop() if stack else ''
                    a = stack.pop() if stack else ''
                    b_g = stack_g.pop() if stack_g else ''
                    a_g = stack_g.pop() if stack_g else ''

                    op = ops.pop()
                    if self.priors[op] < self.priors[ch] or (
                            ch != op and (ch in '+-/') and self.priors[op] == self.priors[ch]):
                        b = "(" + b + ")"
                        b_g = (b_g,)

                    if self.priors[ops.pop()] < self.priors[ch]:
                        a = "(" + a + ")"
                        a_g = (a_g,)

                    ab = a + ch + b
                    ab_g = (a_g, ch, b_g)
                ops.append(ch)
                stack.append(ab)
                stack_g.append(ab_g)
            else:
                var = ch
                # print(var)
                ops.append('')
                stack.append(var)
                stack_g.append(var)
        # print("graph:", stack_g)
        # print("stack:", " ".join(stack))
        # print("end_make_graph".center(25, "_"))
        return stack_g[0]

    def calculate(self, var_values, graph="root_calculation"):
        if graph == 'root_calculation':
            graph = self.graph
        if isinstance(graph, tuple):
            if len(graph) == 1:
                return self.calculate(var_values, graph[0])
            elif len(graph) == 3:
                return self.operators[graph[1]](self.calculate(var_values, graph[0]),
                                                self.calculate(var_values, graph[2]))
            elif len(graph) == 2:
                return self.operators[graph[0]](self.calculate(var_values, graph[1]))
            else:
                raise Exception("Can not calc, bad len")
        else:
            if isinstance(graph, str):
                if is_int(graph):
                    return int(graph)
                elif graph in var_values:
                    return var_values[graph] * (-1.0 if graph[0] == '-' else 1.0)
                else:
                    raise Exception("Can not calc, there is no var in var_values")
            else:
                raise Exception("Can not calc, elem is not int or var")

    def check_operations(self, graph):
        # print("check_operations: ", graph)
        bad_op = (('/', '0'),)
        # print("len:", len(graph))
        if isinstance(graph, tuple):
            if len(graph) == 1:
                return self.check_operations(graph[0])
            elif len(graph) == 2:
                if graph[0] in self.priors:
                    return self.check_operations(graph[1])
                else:
                    raise ("Bad operaion in 2 arg node")
            elif len(graph) == 3:
                # print("checking....: ", (node[1], node[2]))
                if (graph[1], graph[2]) in bad_op:
                    print("FALSE_OP:", graph)
                    return False
                return self.check_operations(graph[0]) and self.check_operations(graph[2])
            else:
                print("FALSE_LEN:", len(graph), graph)
                return False
        else:
            return True


def test1():
    print("Ex_1".center(50, "_"))
    validate_check_list = [
        ('a+2', True),
        ('a-(-2)', True),
        ('a+2-', False),
        ('a+(2+(3+5)', False),
        ('a^2', True),
        ('a^(-2)', True),
        ('-a-2', True),
        ('6/0', False),
        ('a/(b-b)', True),  # мы же не знаем, что b-b это 0, увы
    ]

    suc_count = 0
    for num, (tokens, exp) in enumerate(validate_check_list):
        print("validate_test_{}/{}".center(25, "_").format(num + 1, len(validate_check_list)))
        calc = Calculator(tokens).validate()

        if calc == exp:
            suc_count += 1
        else:
            print('Error in case for "{}". Actual "{}", expected {}'.format(tokens, calc, exp))
    print("".center(25, "_"))
    print("Result {}/{}".format(suc_count, len(validate_check_list)))


def test2():
    print("Ex_2".center(50, "_"))
    str_check_list = [
        ("a", "a"),
        ("-a", "a-"),
        ("(a*(b/c)+((d-f)/k))", "abc/*df-k/+"),
        ("(a)", "a"),
        ("a*(b+c)", "abc+*"),
        ("(a*(b/c)+((d-f)/k))*(h*(g-r))", "abc/*df-k/+hgr-**"),
        ("(x*y)/(j*z)+g", "xy*jz*/g+"),
        ("a-(b+c)", "abc+-"),
        ("a/(b+c)", "abc+/"),
        ("a^(b+c)", "abc+^"),
    ]

    suc_count = 0
    for num, (tokens, exp) in enumerate(str_check_list):
        print("validate_test_{}/{}".center(25, "_").format(num + 1, len(str_check_list)))
        calc = Calculator(tokens)

        if str(calc) == exp:
            suc_count += 1
        else:
            print('Error in case for "{}". Actual "{}", expected {}'.format(tokens, calc, exp))
    print("".center(25, "_"))
    print("Result {}/{}".format(suc_count, len(str_check_list)))


def dev_test():
    # s = "a*(b+(var/(-2+j)+c)+7)"
    var_values = {'a': 5, 'b': 3, 'var': 7, 'c': 1, 'd': 3}
    # s = "b*(4+(a/(-2+1)+a)+var)"
    # s = "-(-var)"
    s = '-(a+b)+c-(-d)'
    print("s:", s)

    calc = Calculator(s)
    print("check_ops:    ", calc.validate())
    print("calculate:    ", calc.calculate(var_values))


test1()
test2()
# dev_test()
