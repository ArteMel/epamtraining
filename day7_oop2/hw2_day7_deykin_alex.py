class EnumMeta(type):
    def __new__(cls, name, bases, dct):
        new_dct = {}
        for attr_name, value in dct.items():
            if attr_name.startswith('_') and attr_name.endswith('_'):
                new_dct[attr_name] = value
            else:
                sub_dct = {**dct}
                sub_dct['name'] = attr_name
                sub_dct['value'] = value
                new_dct[attr_name] = super().__new__(cls, name, bases, sub_dct)
                new_dct[attr_name].__qualname__ = name + '.' + attr_name
        return super().__new__(cls, name, bases, new_dct)

    def __call__(cls, *args, **kwargs):
        if not args:
            super().__call__(cls, *args, **kwargs)
        else:
            for item in iter(cls):
                if item.value == args[0]:
                    return item
            raise ValueError('{} is not a valid {}'.format(args[0], cls.__qualname__))

    def __repr__(cls):
        return "<{}: {}>".format(cls.__qualname__, cls.value)

    def __str__(cls):
        return "<{}: {}>".format(cls.__qualname__, cls.value)

    def __iter__(cls):
        return (value for name, value in cls.__dict__.items() if not (name.startswith('_') and name.endswith('_')))

    def __getitem__(cls, key):
        return getattr(cls, key)


class MyEnum(metaclass=EnumMeta):
    north = 0
    east = 90
    south = 180
    west = 270
    jest = 9


class Direction(MyEnum):
    north = 0
    east = 90
    south = 180
    west = 270

    # 1
    print("Ex_1".center(75, "_"))


try:
    print("Direction.north:      ", Direction.north)  # <Direction.north: 0>
    print("Direction.south:      ", Direction.south)  # <Direction.south: 180>
    print("Direction.north.name: ", Direction.north.name)  # north
    print("Direction.north.value:", Direction.north.value)  # 0
except Exception as ex:
    print(ex)

# 2

print("Ex_2".center(75, "_"))
for i, d in enumerate(Direction):
    print("Elem {}: {}".format(i, d))

# 3
print("Ex_3".center(75, "_"))
try:
    print("id(Direction.north):", id(Direction.north))  # 2171479819208
    print("id(Direction(0)):   ", id(Direction(0)))  # 2171479819208
    print("Direction(30):      ", Direction(30))  # ValueError: 30 is not a valid Direction
except Exception as ex:
    print(ex)

# 4

print("Ex_4".center(75, "_"))
try:
    print("Direction['west']:     ", Direction['west'])  # <Direction.west: 270=0>
    print("Direction['north-west']:", Direction['north-west'])  # KeyError: 'north-west'
except Exception as ex:
    print(ex)
