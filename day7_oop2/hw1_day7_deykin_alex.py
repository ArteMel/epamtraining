class PositiveDescriptor:
    label = '__label'

    def __init__(self, name=None):
        if name:
            self.label = name
            setattr(self, self.label, None)

    def __set__(self, instance, value):
        if value < 0:
            raise Exception('You can set only positive number to ' + self.label + '_value')
        try:
            setattr(instance, self.label, value)
        except:
            pass

    def __get__(self, instance, owner):
        return getattr(instance, self.label)


class BePositive:
    def __init__(self):
        self.inner_value = PositiveDescriptor()

    some_value = PositiveDescriptor('some')
    another_value = PositiveDescriptor('another')


try:
    print("Ex_1".center(75, "_"))
    instance = BePositive()
    # должно работать
    instance.some_value = 1
    print("instance.some_value = ", instance.some_value)

    # должно выкинуть ошибку
    instance.another_value = -1
    print("instance.another_value = ", instance.another_value)

    # должно работать - перезапишется поле класса
    instance.inner_value = -6
    print("instance.inner_value = ", instance.inner_value)
except Exception as ex:
    print(ex)

    print("Ex_2".center(75, "_"))
    print("\n- уровень на котором задаётся дескриптор в самом классе:\
     \nчто если задать дескриптор внутри метода, или вне класса?\
      \nОтвет: перезапишется поле, дескрпитор работать не будет")

try:
    print("Ex_3".center(75, "_"))
    print("\n- проверку инстанции в самом дескрипторе,\
     \nчто если мы создадим несколько объектов этого класса?")
    instance1 = BePositive()
    instance2 = BePositive()
    instance1.some_value = 1
    instance2.some_value = 2

    print("instance1.some_value = ", instance1.some_value)
    print("instance2.some_value = ", instance2.some_value)
except Exception as ex:
    print(ex)

try:
    print("Ex_4".center(75, "_"))
    print("\n- может ли дескриптор принимать какие-то аргументы?\
     \n- как в этом случае мы можем модифицировать получение значений из дескриптора?"
          "\nОтвет: Да может, только при инициаллизации, но не в рантайме, тк есть метод __get__")

except Exception as ex:
    print(ex)
