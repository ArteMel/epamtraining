import collections
import time


class Graph:
    def __init__(self, E):
        self.E = E

    def breadth_first_search(graph, root):
        visited, queue = set(), collections.deque([root])


class GraphIterator:
    def __init__(self, graph, start_v):
        self.graph = graph
        self.start_v = start_v
        self.visited = {vertex: False for vertex in graph.E.keys()}
        self.visited[start_v] = True
        self.queue = collections.deque([start_v])
        self.bfs_list = collections.deque([start_v])
        self.bfs_prepare()

    def bfs_prepare(self):
        while self.queue:
            vertex = self.queue.popleft()
            for neighbour in self.graph.E[vertex]:
                if not self.visited[neighbour]:
                    self.visited[neighbour] = True
                    self.queue.append(neighbour)
                    self.bfs_list.append(neighbour)
            if not self.queue:
                for k, v in self.visited.items():
                    if not v:
                        self.visited[k] = True
                        self.queue.append(k)
                        self.bfs_list.append(k)

    def hasNext(self):
        return bool(self.bfs_list)

    def next(self):
        return self.bfs_list.popleft()


#g = Graph({"A": ["B", "C"], "B": ["A", "C", "D", "E"], "C": ["A", "B", "E"], "D": ["B"], "E": ["B", "C"]})
wrongg = Graph({"A": ["B", "C"], "B": ["A", "C", "D", "E"],"C": ["A", "B", "E"], "D": ["B"], "E": ["B", "C"], "F": ["G"], "G": ["F"]})
start_v = "A"

gi = GraphIterator(wrongg, start_v)
while gi.hasNext():
    print(gi.next())
