import sys, tempfile


def merge_files(fname1: str, fname2: str) -> str:
    with open(fname1, 'r') as f1:
        with open(fname2, 'r') as f2:
            with tempfile.TemporaryFile('w', delete=False) as ft:
                line1 = f1.readline()
                line2 = f2.readline()
                while True:
                    res = ""
                    if not line1:
                        if not line2:
                            break
                        else:
                            res = res + str(int(line2)) + "\n"
                            line2 = f2.readline()
                    else:
                        if not line2:
                            res = res + str(int(line1)) + "\n"
                            line1 = f1.readline()
                        else:
                            n1 = int(line1)
                            n2 = int(line2)

                            if n1 < n2:
                                res = res + str(n1) + "\n"
                                line1 = f1.readline()
                            else:
                                res = res + str(n2) + "\n"
                                line2 = f2.readline()
                    ft.write(res)
                return ft.name


if __name__ == '__main__':
    if len(sys.argv) > 2:
        merge_files(sys.argv[1], sys.argv[2])

fname = merge_files('file1.txt', 'file2.txt')
print(fname)

with open(fname, 'r') as ft:
    while True:
        line = ft.readline()
        if not line: break
        print(line, end="")
