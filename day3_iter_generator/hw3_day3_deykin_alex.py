#!/usr/bin/env python
import os, sys, stat
import glob
from os import walk


def is_hard_link(filename, other):
    s1 = os.stat(filename)
    s2 = os.stat(other)
    return (s1[stat.ST_INO], s1[stat.ST_DEV]) == \
           (s2[stat.ST_INO], s2[stat.ST_DEV])


def hardlink_check(directory_path):
    # print("directory_path", directory_path)
    filenames_all = []
    dirnames_all = []
    dirpath_all = []
    for (dirpath, dirnames, filenames) in walk(directory_path):
        filenames_all.extend([directory_path + "/" + f for f in filenames])
        dirnames_all.extend([directory_path + "/" + d for d in dirnames])
        dirpath_all.append(dirpath)

    # print(filenames_all)
    # print(dirnames_all)
    # print(dirpath_all)
    names_all = filenames_all + dirnames_all
    # print("names_all\n", names_all)

    contain_hard_link_flag = False
    for f1 in names_all:
        for f2 in names_all:
            if f1 == f2:
                continue
            else:
                try:
                    if (is_hard_link(f1, f2)):
                        contain_hard_link_flag = True
                except:
                    pass

    return contain_hard_link_flag




# print("check(./): ", hardlink_check('./'))
print(hardlink_check("./dir1"))
print(hardlink_check("./dir2"))
print(hardlink_check("./dir3"))
print(hardlink_check(""))
print(hardlink_check("./foo/bar"))