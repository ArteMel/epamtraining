def is_armstrong(num):
    nums = str(num)
    return True if num == sum(map(lambda x: int(x) ** len(nums), [*nums])) else False


assert is_armstrong(153) == True, 'Число Армстронга'
assert is_armstrong(10) == False, 'Не число Армстронга'
