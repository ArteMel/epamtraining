import math

sum1 = sum([n**2 for n in range(10 ** 3 + 1)])

sum2 = 0
for i in range(10 ** 6):
    sq = int(math.sqrt(i))
    if sq ** 2 == i:
        sum2 += i

sum3 = sum(filter(lambda x: int(math.sqrt(x)) * int(math.sqrt(x)) == x, range(10 ** 6)))


print(sum1)
print(sum2)
print(sum3)
