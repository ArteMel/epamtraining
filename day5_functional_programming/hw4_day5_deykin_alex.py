import math
import functools

problem9 = \
list(map(lambda x: x[0] * x[1] * x[2], [(a, b, 1000 - a - b) for a in range(1, 1000) for b in range(a, 1000) if
                                        math.sqrt(a ** 2 + b ** 2).is_integer() and int(
                                            math.sqrt(a ** 2 + b ** 2)) == 1000 - a - b and b < 1000 - a - b]))[0]
print("problem9:", problem9)

problem6 = sum(range(101)) ** 2 - sum([x ** 2 for x in range(101)])
print("problem6:", problem6)

problem48 = sum([x ** x for x in range(1, 11)])
problem48 = sum([x ** x for x in range(1, 1001)]) % 10 ** 10
print("problem48:", problem48)

source = ""
i = 0
while len(source) <= 10 ** 6:
    source = source + str(i)
    i += 1

problem40 = functools.reduce(lambda x, y: int(x) * int(y), [source[10 ** i] for i in range(1, 7)])
print("problem40:", problem40)

# Too unoptimise way
problem40line = functools.reduce(lambda x, y: int(x) * int(y),
                                 [functools.reduce(lambda x, y: str(x) + str(y), range(185187))[10 ** i] for i in
                                  range(1, 7)])
print("problem40:", problem40line)
