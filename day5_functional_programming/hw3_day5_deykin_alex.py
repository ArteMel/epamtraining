def collatz_steps(n):
    return 0 if n == 1 else collatz_steps(n // 2 if not n % 2 else n * 3 + 1) + 1


assert collatz_steps(16) == 4
assert collatz_steps(12) == 9
assert collatz_steps(1000000) == 152
