import inspect

def partial(func, *fixated_args, **fixated_kwargs):

    def wrapper(*args, **kwargs):
        return func(*(fixated_args + args), **{**fixated_kwargs, **kwargs})

    wrapper.__name__ = "partial_" + func.__name__
    fastr = ""
    kwastr = ""
    if fixated_args:
        try:
            names = list(inspect.signature(func).parameters.values())
            fastr = ", ".join(["{}={}".format(str(names[i]), val) for i, val in enumerate(fixated_args)])
        except:
            fastr = ", ".join(map(str, fixated_args))


    if fixated_args:
        fastr = fastr + ", "

    kwastr = ", ".join(["{}={}".format(name, str(val)) for name, val in fixated_kwargs.items()])

    wrapper.__doc__ = "A partial implementation of {}\nwith pre-applied arguements being:\n" \
                          .format(func.__name__) + fastr + kwastr

    return wrapper
