def letters_range_naive(end, *args, begin='a', step=1):
    if args:
        begin = end
        end = args[0]
        if len(args) > 1:
            step = args[1]

    if ord(begin) < ord('a') or ord(end) > ord('{'):
        return []

    return map(chr, list(range(ord(begin), ord(end), step)))


