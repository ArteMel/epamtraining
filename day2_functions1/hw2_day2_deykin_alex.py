import inspect
def make_it_count(func, counter_name):
    # print(globals(), "\n\n")
    def wrapper(*args, **kwargs):
        try:
            caller_globals = dict(inspect.getmembers(inspect.stack()[1][0]))["f_globals"]
            glob = caller_globals
        except:
            glob = globals()
        try:
            glob[counter_name] = glob[counter_name] + 1
        except:
            pass
        return func(*args, **kwargs)
    return wrapper

