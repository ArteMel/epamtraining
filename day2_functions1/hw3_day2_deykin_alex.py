def atom(var):
    saved_var = var

    def get_value():
        return saved_var

    def set_value(val):
        nonlocal saved_var
        saved_var = val

    def process_value(*args):
        nonlocal saved_var
        for f in args:
            saved_var = f(saved_var)
        return saved_var

    return get_value, set_value, process_value
