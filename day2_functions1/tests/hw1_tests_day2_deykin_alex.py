# Напишите функцию partial, которая принимает функцию (обозначим ее func),
# а также произвольный набор позиционных (назовем их fixated_args) и именованных
# (назовем их fixated_kwargs) аргументов и возвращет новую функцию,
# которая обладает следующими свойствами:
#
# 1.При вызове без аргументов повторяет поведение функции func, вызванной
# с fixated_args и fixated_kwargs.
# 2.При вызове с позиционными и именованными аргументами дополняет ими
# fixated_args (приписывает в конец списка fixated_args), и fixated_kwargs
# (приписывает новые именованные аргументы и переопределяет значения старых)
# и далее повторяет поведение func с этим новым набором аргументов.
# 3.Имеет __name__ вида partial_<имя функции func>
# 4.Имеет docstring вида:
# """
# A partial implementation of <имя функции func>
# with pre-applied arguements being:
# <перечисление имен и значений fixated_args и fixated_kwargs>
# """
#
# Пояснение.
# partial - удобный способ получать новые функции, реализующие ограниченную
# или специфическую функциональность других функций.
# Например, мы хотим округлять числа с помощью функции round, но
# нас интересует округление всегда только до двух знаков после запятой, поэтому
# мы могли бы сделать так:
# round = partial(round, ndigits=2)
# И теперь round всегда округляет числа так, как нам надо и нам не нужно
# постоянно писать в коде выражения вроде round(n, ndigits=2).
# Конечно в Python уже есть реализация partial, однако в рамках курса,
# мы просим вас сделать собственную реализацию =).
#
# Для того, чтобы получить имена позиционных аргументов, советуем использовать
# возможности модуля inspect.

try:
    from day2.hw1_day2_deykin_alex import partial
except:
    from import_helper import load_src

    load_src("hw1_day2_deykin_alex", "../hw1_day2_deykin_alex.py")
    import hw1_day2_deykin_alex
    from hw1_day2_deykin_alex import partial


def hw1_test():
    print("Testing of {}...".format("HW1").center(75, '_'))
    def tmp(a, b, c, d, e, f=1, g=2, h=3, t=5):
        pass

    tmp2 = partial(tmp, 1, 2, 3, f=8, g=9)
    tmp2(4, 5, h=8, t=9)
    print(tmp2.__name__)
    print(tmp2.__doc__)

    print(round(1 / 3, ndigits=6))
    round2 = partial(round, ndigits=2)
    print(round2(1 / 3))
    print(round2.__name__)
    print(round2.__doc__)


if __name__ == '__main__':
    hw1_test()
