# Напишите реализацию функции atom, которая инкапсулирует некую переменную
# предоставляя интерфейс для получения и изменения ее значения,
# таким образом, что это значение нельзя было бы получить или изменить
# иными способами.
# Пусть функция atom принимает один аргумент, инициализирующий хранимое значение
# (значение по умолчанию, в случае вызова atom без аргумента - None),
# а возвращает 3 функции - get_value, set_value, process_value, такие, что:
#
# get_value - позволяет получить значение хранимой переменной;
# set_value - позволяет установить новое значение хранимой переменной,
# возвращет его;
# process_value - принимает в качестве аргументов сколько угодно функций
# и последовательно (в порядке перечисления аргументов) применяет эти функции
# к хранимой переменной, обновляя ее значение (перезаписывая получившийся
# результат) и возвращая получишееся итоговое значение.

try:
    from day2.hw3_day2_deykin_alex import atom
except:
    from import_helper import load_src

    load_src("hw3_day2_deykin_alex", "../hw3_day2_deykin_alex.py")
    import hw3_day2_deykin_alex
    from hw3_day2_deykin_alex import atom


def hw3_test():
    print("Testing of {}...".format("HW3").center(75, '_'))
    def del9(var):
        return var // 9

    tmpg, tmps, tmpp = atom(5)
    print(tmpg())
    tmps(9999)
    print(tmpg())
    tmpp(del9)
    print(tmpg())
    tmpp(del9, del9, del9)
    print(tmpg())


if __name__ == '__main__':
    hw3_test()
