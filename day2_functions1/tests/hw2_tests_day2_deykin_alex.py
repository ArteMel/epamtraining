# Напишите реализацию функции make_it_count, которая принимает в качестве
# аргументов некую функцию (обозначим ее func) и имя глобальной переменной
# (обозначим ео counter_name), возвращая новую функцию , которая ведет себя
# в точности как функция func, затем исключением, что всякий раз при вызове,
# инкрементирует значение глобальной переменной с именем counter_name.

try:
    from day2.hw2_day2_deykin_alex import make_it_count
except:
    from import_helper import load_src

    load_src("hw2_day2_deykin_alex", "../hw2_day2_deykin_alex.py")
    import hw2_day2_deykin_alex
    from hw2_day2_deykin_alex import make_it_count

counter = 0


def hw2_test():
    print("Testing of {}...".format("HW2").center(75, '_'))
    global str
    global counter
    # print(globals(), "\n\n")
    str = make_it_count(str, "counter")
    print(counter)
    str(3)
    print(counter)
    str(3)
    str(3)
    str(3)
    str(3)
    print(counter)
    str(3)
    str(3)
    print(counter)
    str(3)


if __name__ == '__main__':
    hw2_test()
