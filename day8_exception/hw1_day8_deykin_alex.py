import sys
import io
from functools import wraps

class redirect():
    def __init__(self, dest=None):
        self.dest = dest

    def __enter__(self):
        self.saved_stderr = sys.stderr
        sys.stderr.flush()

        if self.dest:
            self.file = open(self.dest, 'w+')
            sys.stderr = self.file
        else:
            sys.stderr = sys.stdout
        return None

    def __exit__(self, exc_type, exc_val, exc_tb):
        sys.stderr = self.saved_stderr
        if self.dest:
            self.file.close()

        sys.stdout.flush()
        sys.stderr.flush()

def stderr_redirect(dest=None):
    def wrap(f=None):
        @wraps(f)
        def wrapper(*args, **kwargs):
            with redirect(dest) as red:
                result = f(*args, **kwargs)
            return result
        return wrapper
    return wrap


@stderr_redirect(dest="redirector.txt")
def test0():
    print("\ntest0:")
    print("hello redirector out with file")
    print("hello redirector err with file", file=sys.stderr)

@stderr_redirect()
def test1():
    print("\ntest1:")
    print("hello redirector out without file")
    print("hello redirector err without file", file=sys.stderr)

def test2():
    print("\ntest2:")
    print("hello without redirector out")
    sys.stdout.flush()
    print("hello without redirector err", file=sys.stderr)

test0()
print("test0 name: ", test0.__name__)
test1()
test2()

sys.stderr.flush()
sys.stdout.flush()

with open("redirector.txt") as file:
    print("\nredirector.txt:")
    for line in file:
        print("\t", line)