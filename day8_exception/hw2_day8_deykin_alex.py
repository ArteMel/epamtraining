import os
import subprocess

class pidfile:
    def __init__(self, file_name):
        self.file_name = file_name

    def __enter__(self):
        result = subprocess.check_output("ps ax", shell=True).decode('utf8')
        # print("result:\n\"")
        # print(result)
        # print("\"\nfilename:\n")
        # print(self.file_name)

        if self.file_name in result:
            raise ("expects some kind of Exception here")
        else:
            if ".py" in self.file_name:
                with open(self.file_name) as f:
                    # print("exec file")
                    code = f.read()
                    # print("code")
                    # print(code)
                    # exec(code)
                    os.system("python3 " + self.file_name)
            else:
                print("exec code")
                exec(self.file_name)


    def __exit__(self, *exc_info):
        pass


# with pidfile("hw2_test_file.py"):
#     pass
