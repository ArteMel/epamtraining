import subprocess
import os, sys

total_right_count = 0
total_count = 0

def testit(name=None, test_arg_ans=None, exercise_name="smth"):
    global total_right_count
    global total_count
    print("Testing of {}...".format(exercise_name).center(75, '_'))

    if name and test_arg_ans:
        right_count = 0
        for num, el in enumerate(test_arg_ans):
            print("TEST{}".format(num).center(20, '_'))
            # sbout =  exec(open("./hw1_day1_deykin_alex.py").read())
            # sbout = subprocess.call(['python ./hw1_day1_deykin_alex.py', el[0]])
            # sbout = os.system('python ./hw1_day1_deykin_alex.py "{0}"'.format(el[0]))
            proc = subprocess.Popen(["python", name, "{0}".format(el[0])], stdout=subprocess.PIPE,
                                    stderr=subprocess.STDOUT)
            sbout = proc.communicate()[0]
            ans = prepare_str(sbout)
            rans = el[1]

            if ans == rans:
                print("OK!!!")
                right_count += 1
            else:
                try:
                    print("TEST:  '{}'".format(el[0].encode('cp1251').decode('utf8', "ignore")))
                    print("SPOUT: '{}'".format(ans))
                    print("RIGHT: '{}'".format(rans))
                except Exception as ex:
                    print(ex)
                    print("SUS ENCODING: ", sys.getdefaultencoding())


                print("WRONG!!!")

        print("".format(num).center(25, '_'))

        total_right_count += right_count
        total_count += len(test_arg_ans)

        print("RESULT: {}/{}".format(right_count, len(test_arg_ans)))
        print("TOTAL_RESULT: {}/{}".format(total_right_count, total_count))


def common_input(s=None, test_func=None):
    if test_func:
        if not s:
            s = input()
        s = prepare_str(s)
        test_func(s)


def common_input_contin(test_func=None, stop_word="cancel"):
    if test_func:
        while (True):
            s = prepare_str(input())
            test_func(s)
            if s == stop_word:
                return


def prepare_str(s=""):
    if isinstance(s, bytes):
        s = s.decode("utf-8")
    s = s.replace('\n', '')
    s = s.replace('\r', '')
    return s
