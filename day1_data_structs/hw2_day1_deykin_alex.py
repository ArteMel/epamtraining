import sys

try:
    from day1.util import common_input, common_input_contin, prepare_str
except:
    from util import common_input, common_input_contin, prepare_str


def hw2(s):
    stop_word = "cancel"
    if s == stop_word:
        print("Bye!")
        return

    slist = list(map(str.lower, s.split()))
    sdict = dict()
    for word in slist:
        if word not in sdict:
            sdict[word] = 1
        else:
            sdict[word] = sdict[word] + 1

    max_count = 0
    inv_sdict = dict()
    for key, val in sdict.items():
        # print(key, val)
        if val not in inv_sdict:
            inv_sdict[val] = [key]
            max_count = max(max_count, val)
        else:
            inv_sdict[val].append(key)
    # print(max_count)
    # print(inv_sdict[max_count])
    for w in inv_sdict[max_count]:
        try:
            print("{} - {}\n".format(max_count, w))
        except Exception as ex:
            print(ex)



if __name__ == '__main__':
    if len(sys.argv) > 1:
        common_input(s=prepare_str(sys.argv[1]), test_func=hw2)
    else:
        common_input_contin(test_func=hw2)
