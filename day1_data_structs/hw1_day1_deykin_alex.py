import sys

try:
    from day1.util import common_input, prepare_str
except:
    from util import common_input, prepare_str


def hw1(s):
    slist = s.split()
    new_slist = []
    sdict = dict()
    for word in slist:
        if word not in sdict:
            sdict[word] = True
            new_slist.append(word)

    print(" ".join(new_slist))


if __name__ == '__main__':
    if len(sys.argv) > 1:
        common_input(s=prepare_str(sys.argv[1]), test_func=hw1)
    else:
        common_input(test_func=hw1)
