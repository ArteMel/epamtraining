import sys
try:
    from day1.util import common_input, common_input_contin, prepare_str
except:
    from util import common_input, common_input_contin, prepare_str



def RepresentsInt(s):
    try:
        int(s)
        return True
    except ValueError:
        return False


def hw2(s):
    stop_word = "cancel"
    if s == stop_word:
        print("")
        return

    ilist = []
    curi = ""
    for ch in s:
        if RepresentsInt(ch):
            curi += ch
        else:
            if curi and curi != '-':
                #print("xx", curi, "xx")
                ilist.append(int(curi))
                curi = ""
            if ch == '-':
                curi = ch

    if curi:
        ilist.append(int(curi))

    res = 0
    for n in ilist:
        res += n

    print(res)


if __name__ == '__main__':
    if len(sys.argv) > 1:
        common_input(s=prepare_str(sys.argv[1]), test_func=hw2)
    else:
        common_input_contin(test_func=hw2)
