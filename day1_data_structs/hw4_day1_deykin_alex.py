import sys

try:
    from day1.util import common_input_contin, common_input
except:
    from util import common_input_contin, common_input


def hw4_depricated(s):
    if s == "cancel":
        return
    mine = None
    lints = list(map(int, s.split()))
    if lints:
        mine_dict = dict()
        for e in lints:
            if e == mine:
                mine = None
            if e not in mine_dict:
                mine_dict[e] = True
                if e > 1 and e - 1 not in mine_dict:
                    if not mine or e - 1 < mine:
                        mine = e - 1
                elif e + 1 not in mine_dict:
                    if not mine or e + 1 < mine:
                        mine = e + 1
        if 1 not in mine_dict:
            mine = 1

    if mine:
        print(mine)
    # elif maxe:
    # print(maxe + 1)
    else:
        print("none")


def hw4_deprecated2(s):
    if s == "cancel":
        return
    mine = None
    lints = list(map(int, s.split()))
    mine_dict = dict()

    if not lints:
        print("none")
        return

    for e in lints:
        if e == mine:
            mine = None
        if e in mine_dict:
            continue
        mine_dict[e] = True

        if e > 1 and (e - 1 not in mine_dict) and (not mine or e - 1 < mine):
            mine = e - 1
        elif (e + 1 not in mine_dict) and (not mine or e + 1 < mine):
            mine = e + 1

    if 1 not in mine_dict:
        mine = 1

    if mine:
        print(mine)
    # elif maxe:
    # print(maxe + 1)
    else:
        print("none")

    # Аргументация:
    # Доступ к словарю - амортизированная константа D = O(1)
    # Константное количество доп действий за шаг цикла С = O(1)
    # Проходим по списку один раз O(n * (D * C)) = O(n)


def hw4_log(s):
    if s == "cancel":
        return
    lints = list(map(int, s.split()))
    if not lints:
        print("none")
        return

    lints_d = dict()
    lints_onces = []
    for e in lints:
        if e not in lints_d:
            lints_d[e] = True
            lints_onces.append(e)
    lints = lints_onces

    l = 1
    r = max(lints) + 1
    count = 0
    while r - l > 0:
        countl = 0
        countr = 0
        m = (l + r) // 2
        for e in lints:
            countl += e <= m and e >= l
            countr += e > m and e <= r
        if countl < m - l + 1:
            r = m
        else:
            l = m + 1

        # print("\n\nlen:{}\n_____".format(r - l))
        # print('l:{}\n    {}\nm:{}\n    {}\nr:{}'.format(l, countl, m, countr, r))

    print(l)

    # Аргументация:
    # Доступ к словарю - амортизированная константа D = O(1)
    # Удаление дубликатов за один проход плюс побращение к словарю A = O(n)*D
    # Константное количество доп действий за шаг цикла С = O(1)
    # Воспользуемся двоичным поиском O(log n) * C + A = O(n)


def hw4_simplier(s):
    if s == "cancel":
        return
    lints = list(map(int, s.split()))
    mine_dict = {0: True}

    if not lints:
        print("none")
        return

    mine = [1]

    for e in lints:
        mine_dict[e] = True
        mine.append(e - 1)
        mine.append(e + 1)

    mine_new = [e for e in mine if e not in mine_dict]

    print(min(mine_new))


def hw4(s):
    if s == "cancel":
        return
    lints = list(map(int, s.split()))
    mine_dict = {0: True}

    if not lints:
        print("none")
        return

    mine = [1]

    for e in lints:
        mine_dict[e] = True
        mine.append(e - 1)
        mine.append(e + 1)

    mine_new = [e for e in mine if e not in mine_dict]

    print(min(mine_new))


if __name__ == '__main__':
    if len(sys.argv) > 1:
        common_input(s=sys.argv[1], test_func=hw4)
    else:
        common_input_contin(test_func=hw4)
        # hw4('2 1 8 4 2 3 5 10')
