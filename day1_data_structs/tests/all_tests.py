try:
    from day1.tests.hw1_tests_day1_deykin_alex import hw1_test
    from day1.tests.hw2_tests_day1_deykin_alex import hw2_test
    from day1.tests.hw3_tests_day1_deykin_alex import hw3_test
    from day1.tests.hw4_tests_day1_deykin_alex import hw4_test
    # from day1.tests.hw5_tests_day1_deykin_alex import hw5_test
except:
    from hw1_tests_day1_deykin_alex import hw1_test
    from hw2_tests_day1_deykin_alex import hw2_test
    from hw3_tests_day1_deykin_alex import hw3_test
    from hw4_tests_day1_deykin_alex import hw4_test
    # from day1.tests.hw5_tests_day1_deykin_alex import hw5_test

hw1_test()
hw2_test()
hw3_test()
hw4_test()
# hw5_test()
