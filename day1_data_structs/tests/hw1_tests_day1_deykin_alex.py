# Встроенная функция input позволяет ожидать и возвращать данные из стандартного
# ввода в виде строк (весь введенный пользователем текст до нажатия им enter).
# Используя данную функцию, напишите программу, которая:
#
# 1. После запуска предлагает пользователю ввести текст, содержащий любые слова,
# слоги, числа или их комбинации, разделенные пробелом.
# 2. Считывает строку с текстом, и разбивает его на элементы списка, считая
# пробел символом разделителя.
# 3. Печатает этот же список элементов (через пробел), однако с удаленными
# дубликатами.
#
# Пример:
# -> asdfdsf324 ?3 efref4r4 23r(*&^*& efref4r4 a a bb ?3
# asdfdsf324 ?3 efref4r4 23r(*&^*& a bb

try:
    from day1.util import testit
except:
    from import_helper import load_src
    load_src("util", "../util.py")
    import util
    from util import testit

def hw1_test():
    test_arg_ans = [
        (" asdfdsf324 ?3 efref4r4 23r(*&^*& efref4r4 a a bb ?3", "asdfdsf324 ?3 efref4r4 23r(*&^*& a bb"),
        ("sdfsd", "sdfsd")
    ]

    testit("./../hw1_day1_deykin_alex.py", test_arg_ans, "hw1")


if __name__ == '__main__':
    hw1_test()
