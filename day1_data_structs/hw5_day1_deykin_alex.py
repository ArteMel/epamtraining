def palChk(string):
    for c in range(len(string) // 2):
        if string[c] != string[-c - 1]:
            return False
    return True


def testbin():
    for i in range(1, 1000, 100):
        print(bin(i)[2:])


def hw5():
    res = 0
    for n in range(1, 1000001):
        sbin = bin(n)[2:]
        sdec = str(n)
        if palChk(sdec) and palChk(sbin):
            res += n
    print(res)


if __name__ == '__main__':
    # if len(sys.argv) > 1:
    hw5()

