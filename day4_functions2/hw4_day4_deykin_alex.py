import functools
from datetime import datetime
import time


def parametrized(dec):
    def layer(*args, **kwargs):
        def repl(f):
            return dec(f, *args, **kwargs)
        return repl
    return layer


@parametrized
def make_cache(f, delta_time=0):
    storage = {}
    last_time = datetime.now()

    @functools.wraps(f)
    def wrapper(*args, **kwargs):
        nonlocal storage
        cur_time = datetime.now()

        hash = (f, (*args), *tuple(kwargs.items()))
        if hash in storage:
            val, last_time = storage[hash]
        else:
            val = f(*args, **kwargs)
            storage[hash] = (val, cur_time)
            last_time = cur_time

        dt = cur_time - last_time
        dtsec = dt.total_seconds()

        if dtsec > delta_time:
            del storage[hash]

        return val

    return wrapper


@make_cache(delta_time=10)
def fib(n):
    time.sleep(0.03)
    if n < 2:
        return n
    return fib(n - 1) + fib(n - 2)


def test():
    fib10 = make_cache(delta_time=10)(fib)
    fib100 = make_cache(delta_time=100)(fib)

    start_time = datetime.now()
    print(fib10(200))
    print("DTIME: ", (datetime.now() - start_time).total_seconds())

    start_time = datetime.now()
    print(fib100(200))
    print("DTIME: ", (datetime.now() - start_time).total_seconds())


test()
