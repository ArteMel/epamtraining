try:
    from day4.hw2_day4_deykin_alex import *
except:
    from hw2_day4_deykin_alex import *

from math import log10, sqrt
from random import random


def mysqrt_test(n, eps=None):
    x = abs(newton(n))

    if eps:
        prec = int(log10(int(1 / eps)))
    else:
        prec = eval_prec(x)
    form = '.' + str(prec) + 'f'
    # print(format(x, form))
    return x, prec


def prec_test(arg, prec=None):
    try:
        arg2 = arg ** 2
        orig = sqrt(arg2)
        if prec:
            eps = 1 / 10 ** prec
            my, _ = mysqrt_test(arg2, eps)
        else:
            my, prec = mysqrt_test(arg2)
            eps = 1 / 10 ** prec

        mys = str(my)
        origs = str(orig)
        if not prec is None:
            doti = mys.find('.')
            preci = (0 if not prec else prec + 1)
            catres = min(doti + preci, len(mys), len(origs))
            mystr = mys[:catres]
            origstr = origs[:catres]
        else:
            mystr = mys
            origstr = origs[:len(mystr)]

        if mystr == origstr:
            return True
        else:
            print("prec {}".center(30, '_').format(prec))
            print("arg:   ", arg)
            print("arg2:  ", arg2)
            # print("orig:  ", origs)
            # print("my:    ", mys)
            print("sqrt:  ", origstr)
            print("MYSQRT:", mystr)
            precformat = '{:0.' + str(prec) + 'f}'
            print(("eps:    " + precformat).format(eps))
            print("wrong!!")
            return False
    except Exception as ex:
        print("exception".center(30, '_'))
        print(ex)
        print("arg: ", arg)
        print("prec:", prec)
        return False


def prec_tests():
    n = 5000
    okcount = 0
    prec_count = 18
    total_count = 0
    for cur_prec in range(prec_count):
        for i in range(0, n):
            ii = (((i / (n + random()) or 1) / ((random() ** (1 / random())) or 1) or 1) % 10 ** 35) or 1
            ii = ii if ii == ii else 1
            if prec_test(ii, cur_prec):
                okcount += 1

            total_count += 1

    print("RESULTS".center(40, '_'))
    print("ok:    {}\ntotal: {}\nwrong:  {}".format(okcount, total_count, total_count - okcount))


prec_tests()
