import functools
from datetime import datetime
import time
import math


def parametrized(dec):
    def layer(*args, **kwargs):
        def repl(f):
            return dec(f, *args, **kwargs)

        return repl

    return layer


@parametrized
def profile_dec(f, name="none"):
    if not name in globals():
        globals()[name] = (0, 0)

    @functools.wraps(f)
    def wrapper(*args, **kwargs):
        begin_time = datetime.now()
        ret = f(*args, **kwargs)
        end_time = datetime.now()
        dtsec = (end_time - begin_time).total_seconds()
        cur_val = globals()[name]
        globals()[name] = (cur_val[0] + 1, cur_val[1] + dtsec)
        return ret

    return wrapper


@profile_dec(name="mysleepstor")
def mysleep(t=0, cycles=1):
    for i in range(cycles):
        time.sleep(t / cycles)


@profile_dec(name="fib1_stor")
def fib1(n):
    if n < 2:
        return n
    return fib1(n - 1) + fib1(n - 2)

@profile_dec(name="fib2_stor")
def fib2(n):
    SQRT5 = math.sqrt(5)
    PHI = (SQRT5 + 1) / 2
    return int(PHI ** n / SQRT5 + 0.5)

@profile_dec(name="fib3_stor")
def fib3(n):
    a = 0
    b = 1
    for __ in range(n):
        a, b = b, a + b
    return a


def test():
    # print(fib(25))
    # print("count:{}\ntime:{}".format(storage[0], storage[1]))
    #
    # print(mysleep(5))
    # print("count:{}\ntime:{}".format(mysleepstor[0], mysleepstor[1]))
    #
    # print(mysleep(5, 10000))
    # print("count:{}\ntime:{}".format(mysleepstor[0], mysleepstor[1]))

    arg = 26
    n = 1000
    times = [0.0, 0.0, 0.0]
    vals = [0,0,0]
    for i in range(10):

        # vals[0] = fib1(arg)
        vals[1] = fib2(arg)
        vals[2] = fib3(arg)

        times[0] = times[0] + fib1_stor[1]
        times[1] = times[1] + fib2_stor[1]
        times[2] = times[2] + fib3_stor[1]

    print("fib1")
    print("val: ", vals[0])
    print("count:{}\ntime:{}\n".format(fib1_stor[0], times[0]))

    print("fib2")
    print("val: ", vals[1])
    print("count:{}\ntime:{}\n".format(fib2_stor[0], times[1]))

    print("fib3")
    print("val: ", vals[2])
    print("count:{}\ntime:{}\n".format(fib3_stor[0], times[2]))

test()
