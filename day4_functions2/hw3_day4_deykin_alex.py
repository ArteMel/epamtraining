def count_points(a, b, c):
    n = 0
    minx = min(a[0], b[0], c[0])
    miny = min(a[1], b[1], c[1])
    maxx = max(a[0], b[0], c[0])
    maxy = max(a[1], b[1], c[1])

    for x in range(minx, maxx + 1):
        for y in range(miny, maxy + 1):
            if check_pint(a, b, c, (x, y)):
                n += 1
    print(n)
    return n


def check_pint(a, b, c, p):
    if (a[0] >= b[0] and a[1] >= b[1]):
        a, b, c = b, c, a

    bx = b[0] - a[0]
    by = b[1] - a[1]
    cx = c[0] - a[0]
    cy = c[1] - a[1]
    px = p[0] - a[0]
    py = p[1] - a[1]
    m = (px * by - bx * py) / ((cx * by - bx * cy) or 1);
    if (m >= 0) and (m <= 1):
        l = (px - m * cx) / (bx or 1)
        if (l >= 0) and ((m + l) <= 1):
            return True

    return False

print("___")
count_points((0, 0), (1, 0), (0, 1))
count_points((1, 0), (0, 1), (0, 0))
count_points((0, 1), (0, 0), (1, 0))
print("___")

count_points((1, 1), (1, 0), (0, 1))
count_points((1, 0), (0, 1), (1, 1))
count_points((0, 1), (1, 1), (1, 0))
print("___")

count_points((2, 0), (0, 2), (0, 0))
count_points((0, 2), (0, 0), (2, 0))
count_points((0, 0), (2, 0), (0, 2))
print("___")

count_points((2, 2), (2, 0), (0, 2))
count_points((2, 0), (0, 2), (0, 0))
count_points((0, 0), (2, 0), (0, 2))
print("___")


count_points((-2, -5), (0, 0), (5, 2))
count_points((5, 2), (0, 0), (-2, -5))
count_points((5, 2), (-2, -5), (0, 0))
print("___")

count_points((0, 1), (0, 0), (-8, 0))
count_points((0, 0), (0, 3), (4, 0))
count_points((0, 0), (0, 0), (0, 0))