from math import log10, sqrt


def eval_prec(n):
    s = str(n)
    s = s[2:]
    s = s[::-1]
    while s and s[0] == '0':
        s = s[1:]
    return len(s)


def f(x, n):
    return n - x ** 2


def df(x):
    return -2 * x


def newton(n):
    x0 = n / 2
    xn1 = f(x0, n)
    # xn1 = xn - f(xn, n) / (df(xn) or 1.0)
    while True:
        xn = xn1
        xn1 = xn - f(xn, n) / (df(xn) or 1.0)
        last_delta = delta if 'delta' in locals() else 1.0
        delta = abs(f(xn1, n) - f(xn, n))
        # print('  delta:', delta)
        # print('    xn1:', xn1)
        if str(last_delta)==str(delta): break
    print('\nx before precision round:', xn1)
    return xn1


def mysqrt(n, eps=None):
    x = abs(newton(n))
    if eps:
        prec = int(log10(int(1 / eps)))
    else:
        prec = eval_prec(x)
    form = '.' + str(prec) + 'f'
    return float(format(x, form))


print(mysqrt(4))
print(sqrt(4))
print(mysqrt(5, 0.01))
print(sqrt(5))
print(mysqrt(4 / 10000))
print(sqrt(4 / 10000))
print(mysqrt(4 / 100000000000000))
print(sqrt(4 / 100000000000000))
print(mysqrt(4 * 24))
print(sqrt(4 * 24))
print(mysqrt(4e-256))
print(sqrt(4e-256))
