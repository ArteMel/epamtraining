import sys

try:
    from day4.util import common_input, common_input_contin, prepare_str
except:
    from util import common_input_contin, prepare_str


def hw1(input_string):
    stop_word = "cancel"
    if input_string == stop_word:
        print("")
        return
    is_int, result = to_int(input_string)
    if is_int:
        if result % 2:
            result = result * 3 + 1
        else:
            result = result // 2
        print(result)
    else:
        print("Не удалось преобразовать введенный текст в число.")


def to_int(input_string):
    if input_string:
        without_last_ch = input_string[:len(input_string) - 1]
        flag, result = to_int(without_last_ch)
        ch = input_string[-1]
        # print("ch: ", ch)
        if flag and ord('0') <= ord(ch) and ord(ch) <= ord('9'):
            n_last = (ord(ch) - ord('0'))
            if result < 0:
                n_last = -1 * n_last
            if result == float('-inf'):
                result = 0
            return True, result * 10 + n_last
        elif input_string == '-':
            return True, float('-inf')
        else:
            return False, 0
    else:
        return True, 0


def test():
    hw1('2')
    hw1('3')
    hw1('Два')
    hw1('cancel')

test()

#
# if __name__ == '__main__':
#     if len(sys.argv) > 1:
#         common_input_contin(s=prepare_str(sys.argv[1]), test_func=hw1)
#     else:
#         common_input_contin(test_func=hw1)

